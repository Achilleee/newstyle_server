#ifndef __INC_WOM2_SHOP_H__
#define __INC_WOM2_SHOP_H__

const int ACTIVITY_CAP = 100;

inline int GetOfflineShopMinutesFromActivity(int activity, bool hasPremium)
{
	int rate = hasPremium ? 2 : 1;
	return MINMAX(0, (int)(60 * (activity * 1.0 / ACTIVITY_CAP)) * rate, (60 * 8) * rate);
}

#endif
