#ifndef __INC_METIN_II_DB_SHOP_H__
#define __INC_METIN_II_DB_SHOP_H__

#include <memory>
#include "Peer.h"

class Shop
{
public:
	Shop();
	void Initialize();

	bool HasPeer() const { return m_linkedPeer != nullptr; };
	void BindPeer(CPeer* peer) { m_linkedPeer = peer; };
	CPeer* GetPeer() const { return m_linkedPeer; };

	void SetTable(const TPlayerShopTable &newTab) { m_table = newTab; }
	TPlayerShopTable* GetTable() { return &m_table; }

	void SetItem(int index, TShopItemTable table);

	//Info getters
	DWORD GetOwnerPID() const { return m_table.pid; }

	void SetClosed(bool status) { m_table.closed = status; }
	bool IsClosed() const { return m_table.closed; }

	bool HasItems() const;

	void SetOfflineMinutes(int minutes) { m_offlineMinutesLeft = minutes; }
	int GetOfflineMinutes() { return m_offlineMinutesLeft; }

	void SetPremiumMinutes(int minutes) { m_premiumMinutesLeft = minutes; }
	int GetPremiumMinutes() { return m_premiumMinutesLeft; }

	void SetOfflineMinutesFromActivity(int activity, bool premium = false);

	void AlterGoldStash(DWORD price, bool bAdd);
	void SetGoldStash(DWORD stash) { m_goldStash = m2::minmax<DWORD>(0, stash, UINT_MAX); }
	DWORD GetGoldStash() const { return m_goldStash; };

	TPlayerShopTableCache GetCacheTable();

	void SetName(std::string& newName) { strlcpy(m_table.shopName, newName.c_str(), sizeof(m_table.shopName)); }

	bool IsOwnerOnline() const { return m_isOwnerOnline; }
	void SetOwnerOnline(bool value) { m_isOwnerOnline = value; }

	bool RemoveItem(int display_pos);
	int FindItem(int display_pos);

private:
	TPlayerShopTable m_table;
	DWORD m_goldStash;
	int m_offlineMinutesLeft;
	int m_premiumMinutesLeft;

	CPeer* m_linkedPeer;
	bool m_isOwnerOnline;
};

typedef std::shared_ptr<Shop> spShop;

#endif
