#include "stdafx.h"
#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
#include "../../common/shop_helper.h"
#include "Shop.h"
#include "ClientManager.h"

Shop::Shop() : m_goldStash(0), m_offlineMinutesLeft(0), m_premiumMinutesLeft(0), m_linkedPeer(nullptr), m_isOwnerOnline(false)
{
	Initialize();
}

void Shop::Initialize()
{
	m_linkedPeer = nullptr;
	memset(&m_table, 0, sizeof(m_table));
}

void Shop::SetItem(int index, TShopItemTable table)
{
	m_table.items[index] = table;
}

bool Shop::HasItems() const
{
	for (const auto item : m_table.items)
	{
		if (item.vnum != 0)
			return true;
	}

	return false;
}

void Shop::AlterGoldStash(DWORD price, bool bAdd)
{
	if (bAdd)
		m_goldStash += price;
	else
		m_goldStash -= price;

	//Limit goldstash values:
	m_goldStash = m2::minmax<DWORD>(0, m_goldStash, 2000000000);
}

void Shop::SetOfflineMinutesFromActivity(int activity, bool premium /*= false*/)
{
	SetOfflineMinutes(GetOfflineShopMinutesFromActivity(activity, premium));
}

//Form the table that is required to save to cache.
TPlayerShopTableCache Shop::GetCacheTable()
{
	TPlayerShopTable* table = GetTable();

	TPlayerShopTableCache cacheTable;
	cacheTable.pid = table->pid;
	cacheTable.channel = table->channel;
	cacheTable.closed = table->closed;
	cacheTable.goldStash = GetGoldStash();
	thecore_memcpy(cacheTable.items, table->items, sizeof(cacheTable.items));
	thecore_memcpy(cacheTable.playerName, table->playerName, sizeof(cacheTable.playerName));
	thecore_memcpy(cacheTable.shopName, table->shopName, sizeof(cacheTable.shopName));
	cacheTable.mapIndex = table->mapIndex;
	cacheTable.offlineMinutesLeft = GetOfflineMinutes();
	cacheTable.premiumMinutesLeft = GetPremiumMinutes();
	cacheTable.x = table->x;
	cacheTable.y = table->y;
	cacheTable.openTime = table->openTime;

	return cacheTable;
}

bool Shop::RemoveItem(int display_pos)
{
	int found = false;

	TShopItemTable newItemTable[45];
	memset(newItemTable, 0, sizeof(newItemTable));
	for (int i = 0, k = 0; i < SHOP_HOST_ITEM_MAX_NUM; ++i)
	{
		TShopItemTable item = m_table.items[i];
		if (item.vnum == 0)
			continue;

		if (item.display_pos == display_pos)
		{
			found = true;
			continue;
		}

		newItemTable[k++] = item;
	}

	thecore_memcpy(m_table.items, newItemTable, sizeof(m_table.items));
	return found;
}

int Shop::FindItem(int display_pos)
{
	for (int i = 0; i < SHOP_HOST_ITEM_MAX_NUM; ++i)
	{
		TShopItemTable item = m_table.items[i];
		if (item.vnum == 0)
			continue;

		if (item.display_pos == display_pos)
			return i;
	}

	return -1;
}
#endif
