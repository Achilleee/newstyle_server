#ifndef __INC_METIN_II_GAME_SHOP_H__
#define __INC_METIN_II_GAME_SHOP_H__

enum
{
	SHOP_MAX_DISTANCE = 1000
};

#include "../../common/CommonDefines.h"

class CGrid;

/* ---------------------------------------------------------------------------------- */
class CShop
{
	public:
		typedef struct shop_item
		{
			DWORD	vnum;		// 아이템 번호
			long	price;		// 가격
			BYTE	count;		// 아이템 개수

			LPITEM	pkItem;
			int		itemid;		// 아이템 고유아이디

			shop_item()
			{
				vnum = 0;
				price = 0;
				count = 0;
				itemid = 0;
				pkItem = NULL;
			}
		} SHOP_ITEM;

		CShop();
		virtual ~CShop(); // @fixme139 (+virtual)

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		static bool CanOpenShopHere(long mapindex);
#endif

		bool	Create(DWORD dwVnum, DWORD dwNPCVnum, TShopItemTable * pItemTable);

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		void	TransferItems(LPCHARACTER owner, TShopItemTable * pTable, BYTE bItemCount);
		void	SetShopItem(TShopItemTable * pItemTable);
#endif

		void	SetShopItems(TShopItemTable * pItemTable, BYTE bItemCount);

		LPCHARACTER GetOwner() { return  m_pkPC;  };

		virtual void	SetPCShop(LPCHARACTER ch);
		virtual bool	IsPCShop()	{ return m_pkPC ? true : false; }

		// 게스트 추가/삭제
		virtual bool	AddGuest(LPCHARACTER ch,DWORD owner_vid, bool bOtherEmpire);
		void	RemoveGuest(LPCHARACTER ch);

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		virtual int Buy(LPCHARACTER ch, BYTE pos, bool isSearchBuy = false);
#else
		// 물건 구입
		virtual int	Buy(LPCHARACTER ch, BYTE pos);
#endif

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		bool TransferItemAway(LPCHARACTER ch, BYTE pos, TItemPos targetPos);
		bool RemoveItemByID(DWORD itemID);
#endif

		// 게스트에게 패킷을 보냄
		void	BroadcastUpdateItem(BYTE pos);

		// 판매중인 아이템의 갯수를 알려준다.
		int		GetNumberByVnum(DWORD dwVnum);

		// 아이템이 상점에 등록되어 있는지 알려준다.
		virtual bool	IsSellingItem(DWORD itemID);

		DWORD	GetVnum() { return m_dwVnum; }
		DWORD	GetNPCVnum() { return m_dwNPCVnum; }

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		void SetClosed(bool status) { m_closed = status; }
		bool IsClosed() const { return m_closed; }

		void SetOpenTime(DWORD time) { m_openTime = time; }
		DWORD GetOpenTime() const { return m_openTime; }

		void SetOfflineMinutes(int mins) { m_offlineMinutes = mins; }
		int GetOfflineMinutes() const { return m_offlineMinutes; }

		void SetPremiumMinutes(int mins) { m_premiumMinutes = mins; }
		int GetPremiumMinutes() const { return m_premiumMinutes;  }

		std::string GetShopSign() { return m_sign; }
		void		SetShopSign(std::string sign) { m_sign = sign; }
		std::vector<SHOP_ITEM>	GetItemVector() const { return m_itemVector; }

		void SetNextRenamePulse(int pulse) { m_renamePulse = pulse; }
		int	 GetRenamePulse() { return m_renamePulse; }

		void Save();
		void SaveOffline();

		bool	IsEmpty();
#endif

	protected:
		void	Broadcast(const void * data, int bytes);
#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		void	CloseMyShop();
#endif

	protected:
		DWORD				m_dwVnum;
		DWORD				m_dwNPCVnum;

		CGrid *				m_pGrid;

		typedef TR1_NS::unordered_map<LPCHARACTER, bool> GuestMapType;
		GuestMapType m_map_guest;
		std::vector<SHOP_ITEM>		m_itemVector;	// 이 상점에서 취급하는 물건들

		LPCHARACTER			m_pkPC;

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
		std::string m_sign;
		bool	m_closed;

		DWORD m_openTime;
		int m_offlineMinutes;
		int m_premiumMinutes;
		int m_renamePulse;
#endif
};

#endif
