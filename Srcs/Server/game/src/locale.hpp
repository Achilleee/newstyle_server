#ifndef __INC_METIN2_GAME_LOCALE_H__
#define __INC_METIN2_GAME_LOCALE_H__

#ifdef ENABLE_MULTI_LANGUAGE_SYSTEM
#include "../../common/CommonDefines.h"
#endif

extern "C"
{
#ifdef ENABLE_MULTI_LANGUAGE_SYSTEM
	void locale_init(std::string lang, const char *filename);
	const char *locale_find(const char *string, std::string lang = "default");
#else
	void locale_init(const char *filename);
	const char *locale_find(const char *string);
#endif

	extern int g_iUseLocale;

#define LC_TEXT(str) locale_find(str)

#ifdef ENABLE_MULTI_LANGUAGE_SYSTEM
#define LC_TEXT_TRANS(str, lang) locale_find(str, lang)
#endif
};

#endif
