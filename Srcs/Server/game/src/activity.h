#ifndef __INC_METIN_II_ACTIVITY_H__
#define __INC_METIN_II_ACTIVITY_H__

#include <array>
#include <memory>

#include "../../common/tables.h"
#include "../../common/shop_helper.h"

const int MOB_KILLS_FOR_ACTIVITY_AWARD = 20;
const int MAX_ACTIVITY = ACTIVITY_CAP * 10;
const int REMEMBER_PVP_KILLS = 10;

enum ActivityType
{
	ACTIVITY_PVP, //Kingdom vs Kingdom & Player vs Player
	ACTIVITY_GK, //Guild
	ACTIVITY_PVM, //Metins, bosses, monster killing
	ACTIVITY_OTHER_PVE, //Fishing, mining

	MAX_ACTIVITY_TYPE
};

class ActivityHandler
{
	public:
		static std::map<DWORD, std::vector<DWORD>> killMap; //<pid, {killedPID, killedPID2, ..., killedPID10}>
		static bool CountsForActivity(LPCHARACTER killer, LPCHARACTER victim);
		static void RecordKill(DWORD killerPID, DWORD victimPID);

	public:
		ActivityHandler(LPCHARACTER player);

		//Makers
		void MarkMonsterKill(LPCHARACTER mob);
		void MarkFishing(bool success);
		void MarkPlayerKill(LPCHARACTER killed, bool isUnderGW);
		void MarkMining();

		//Loading
		void Load(TActivityTable * data);

		//Setters & getters
		int GetActivity() const { return m_activityTotal; };
		void AlterActivity(int var) { m_activityTotal += var; };

		//Saver
		void Save();

	private:
		//Helpers
		double ActivityDeltaByLevelDiff(int diff);

		//Binding character
		LPCHARACTER m_player;
		LPCHARACTER GetPlayer() const { return m_player; }

		//Control activity
		void AddPoints(ActivityType type, double am);
		void ActivityTimeCheck(const time_t curTime);
		void SyncShopOfflineTime();

		std::array<int, MAX_ACTIVITY_TYPE> m_activityToday;
		int m_activityTotal;
		int m_monstersKilled;
		time_t m_lastUpdate;

		/*
			Whether we sould be sending a packet to the server on Save() or not.
			Set to true when something is changed, set to false while there are no changes.
		*/
		bool m_doSave;
};

typedef std::shared_ptr<ActivityHandler> spActivityHandler;

#endif
