#include "stdafx.h"

#ifdef ENABLE_OFFLINE_SHOP_SYSTEM
#include <ctime>

#include "activity.h"
#include "char.h"
#include "shop.h"
#include "desc_client.h"
#include "../../common/tables.h"

std::map<DWORD, std::vector<DWORD>> ActivityHandler::killMap;

ActivityHandler::ActivityHandler(LPCHARACTER player) : m_activityTotal(0), m_lastUpdate(std::time(nullptr)), m_doSave(false)
{
	m_player = player;
	m_activityToday = {{ 0 }};
};

/*
Shops
Shops now survive maintenances.
Shops can now be open while you are offline.
Actions in the game (such as fishing, mining, killing monsters, PvP, guild wars...) increase the time that your shop can be online.
There's a maximum time (1h) you can obtain by doing those actions every day.
Every day that goes by, you will lose a bit of activity – which means that you will lose a bit of offline time. You can quickly recover it by doing some of the actions outlined above.
The maximum offline time you can reach with this method is 8h. You can extend this time with “Hourglass' Sand” and “Blessed Clock Hand” up to 56h. Both items will be made available on the itemshop sometime today.
After the offline time runs out, they will close. When you log back in, they will spawn right away!
Shop opening has been restricted to certain maps: Hometown and second village of each empire. For exchanges outside of those areas you should use trades.
Shops are now separate from the player – you can continue playing even after creating a shop.
Increased shop size: Shops can now hold a full inventory page.
The items you put on your shop will be gone from your inventory.
Added a new button on inventory, the shop window, which allows you to see everything you have in your shop from anywhere, as well as the shop's location and the Yang inside it.
Shop items can be added and removed anytime, so long as the player is nearby the shop.
Shops can no longer be attacked, and players do not collide with them.
Shops are now displayed in the minimap AND large map in an orange color.
Shops now have a Yang storage of their own where all the sales Yang goes. You can withdraw this Yang from anywhere, anytime. A reminder will display after you've been logged out for a while so that you don't forget it there.
Shops will now get closed automatically when they have no more items left.
Blocked Bundle from being sold in shops.
You no longer need to remove your armor to open a shop.
*/


/*
//////////////////////////////////////////
////////////////////////
#1) MONSTER KILL:
////////////////////////
//////////////////////////////////////////
	delta = ActivityDeltaByLevelDiff(LIVELLO_PG - LIVELLO_MOB) (guardare punto 5 per la funzione: ActivityDeltaByLevelDiff)

	- METIN:
		AddPoints(100 / 250 * delta);

	- BOSS:
		AddPoints(1 / (3 + 5 - mob->GetMobRank()) * 100 / 100 * delta);

	- MOB (ogni 20 uccisioni):
		AddPoints(1 / 5 * 100 / 100 * delta);
//////////////////////////////////////////



//////////////////////////////////////////
////////////////////////
#2) PLAYER KILL:
////////////////////////
//////////////////////////////////////////
	delta = ActivityDeltaByLevelDiff(LIVELLO_PG_UCCISORE - LIVELLO_PG_VITTIMA) (guardare punto 5 per la funzione: ActivityDeltaByLevelDiff)
	AddPoints(100 * 0.5 / 100 * delta);
//////////////////////////////////////////



//////////////////////////////////////////
////////////////////////
#3) PESCA:
////////////////////////
//////////////////////////////////////////
	- PESCA RIUSCITA:
		delta = 1 / 12
		AddPoints(100 * delta);

	- PESCA FALLITA:
		delta = 1 / 50
		AddPoints(100 * delta);
//////////////////////////////////////////



//////////////////////////////////////////
////////////////////////
#4) PICCONAGGIO:
////////////////////////
//////////////////////////////////////////
	AddPoints(100 * 1 / 10);
//////////////////////////////////////////



#5) ActivityDeltaByLevelDiff(DIFF TRA LIV)
{
	//DIFF TRA LIV		PT.
	-15					100,
	-14					98,
	-13					94,
	-12					91,
	-11					89,
	-10					86,
	-9					80,
	-8					76,
	-7					72,
	-6					68,
	-5					61,
	-4					54,
	-3					48,
	-2					40,
	-1					33,
	0					33,
	+1					30,
	+2					28,
	+3					26,
	+4					24,
	+5					22,
	+6					20,
	+7					18,
	+8					16,
	+9					14,
	+10					12,
	+11					5,
	+12					4,
	+13					3,
	+14					2,
	+15					1,
}



//////////////////////////////////////////
//////////////////////////////////////////
////////////////////////
#6) CALCOLO TEMPO SHOP OFFLINE IN MINUTI
////////////////////////
	60 * (RISULTATO_AddPoints / 100)
//////////////////////////////////////////
//////////////////////////////////////////
*/


void ActivityHandler::MarkMonsterKill(LPCHARACTER mob)
{
	double delta = ActivityDeltaByLevelDiff(GetPlayer()->GetLevel() - mob->GetLevel());

	//Metin
	if (mob->IsStone())
	{
		AddPoints(ACTIVITY_PVM, ACTIVITY_CAP * 1.0 / 250.0 * delta);
	}
	//Boss
	else if (mob->GetMobRank() >= MOB_RANK_BOSS)
	{
		// Lv 5 mob will give 1/3 * cap in best case scenario
		// Boss will give 1/4 * cap in best case scenario

		AddPoints(ACTIVITY_PVM, 1.0 / (3 + 5 - mob->GetMobRank()) * ACTIVITY_CAP * 1 / 100 * delta);
	}
	//No boss nor metin
	else
	{
		++m_monstersKilled;
		if (m_monstersKilled > MOB_KILLS_FOR_ACTIVITY_AWARD)
		{
			m_monstersKilled = 0;
			AddPoints(ACTIVITY_PVM, 1.0 / 5.0 * (double)ACTIVITY_CAP * 1.0 / 100.0 * delta);
		}
	}
}

void ActivityHandler::MarkPlayerKill(LPCHARACTER victim, bool isUnderGW)
{
	if (!victim)
	{
		sys_err("Killed nullptr!");
		return;
	}

	if (!CountsForActivity(GetPlayer(), victim))
		return;

	AddPoints(isUnderGW ? ACTIVITY_GK : ACTIVITY_PVP, ACTIVITY_CAP * 0.5 / 100 * ActivityDeltaByLevelDiff(GetPlayer()->GetLevel() - victim->GetLevel()));
	RecordKill(GetPlayer()->GetPlayerID(), victim->GetPlayerID());
}

void ActivityHandler::MarkFishing(bool success)
{
	double rate = success ? 1.0 / 12 : 1.0 / 50;
	AddPoints(ACTIVITY_OTHER_PVE, ACTIVITY_CAP * rate);
}

void ActivityHandler::MarkMining()
{
	AddPoints(ACTIVITY_OTHER_PVE, ACTIVITY_CAP * 1 / 10);
}

void ActivityHandler::Load(TActivityTable * data)
{
	m_activityToday[ACTIVITY_PVP] = data->today.pvp;
	m_activityToday[ACTIVITY_GK] = data->today.gk;
	m_activityToday[ACTIVITY_PVM] = data->today.pve;
	m_activityToday[ACTIVITY_OTHER_PVE] = data->today.other;

	m_activityTotal = data->total;
	m_lastUpdate = data->lastUpdate;

	//Run a quick activity check (daily loss/counter reset)
	time_t now = std::time(nullptr);

	ActivityTimeCheck(now);
	SyncShopOfflineTime();
}

/*
	diff is expected to be MYLEVEL - TARGET
*/
double ActivityHandler::ActivityDeltaByLevelDiff(int diff)
{
	//Negative difference is a good thing. We are below the target's level
	diff = MINMAX(-15, diff, 15);

	int delta[] =
	{
		100,	//-15
		98,		//-14
		94,		//-13
		91,		//-12
		89,		//-11
		86,		//-10
		80,		//-9
		76,		//-8
		72,		//-7
		68,		//-6
		61,		//-5
		54,		//-4
		48,		//-3
		40,		//-2
		33,		//-1
		33,		//0
		30,		//+1
		28,		//+2
		26,		//+3
		24,		//+4
		22,		//+5
		20,		//+6
		18,		//+7
		16,		//+8
		14,		//+9
		12,		//+10
		5,		//+11
		4,		//+12
		3,		//+13
		2,		//+14
		1,		//+15
	};

	return delta[diff + 15];
}

void ActivityHandler::AddPoints(ActivityType type, double am)
{
	if (type >= MAX_ACTIVITY_TYPE)
		return;

	//Current time
	time_t now = std::time(nullptr);
	ActivityTimeCheck(now);

	//Add the points
	int iAm = MINMAX(0, (int)am, ACTIVITY_CAP - m_activityToday[type]);
	if (iAm < 1)
	{
		sys_log(1, "Activity cap already reached for type %d (Trying to add %d on player %s)", type, iAm, GetPlayer()->GetName());
		return;
	}

	m_activityToday[type] += iAm;
	m_activityTotal = MINMAX(0, m_activityTotal + iAm, MAX_ACTIVITY);

	//Set last update to now
	m_lastUpdate = now;

	SyncShopOfflineTime();

	//Mark to save
	m_doSave = true;
}

void ActivityHandler::Save()
{
	if (!m_doSave)
		return;

	TActivityTable table;
	table.pid = GetPlayer()->GetPlayerID();
	table.lastUpdate = m_lastUpdate;

	table.today.pve = m_activityToday[ACTIVITY_PVM];
	table.today.pvp = m_activityToday[ACTIVITY_PVP];
	table.today.gk = m_activityToday[ACTIVITY_GK];
	table.today.other = m_activityToday[ACTIVITY_OTHER_PVE];

	table.total = std::min(m_activityTotal, MAX_ACTIVITY);

	db_clientdesc->DBPacket(HEADER_GD_SAVE_ACTIVITY, 0, &table, sizeof(TActivityTable));

	//No need to save again until something changes
	m_doSave = false;
}

void ActivityHandler::ActivityTimeCheck(const time_t timestamp)
{
	//If different year, or different day of the year...
	tm curTime = *std::gmtime(&timestamp);
	tm lastUpdate = *std::gmtime(&m_lastUpdate);

	if (curTime.tm_year > lastUpdate.tm_year || curTime.tm_yday > lastUpdate.tm_yday)
	{
		//...reset today's activity caps
		m_activityToday[ACTIVITY_PVM] = 0;
		m_activityToday[ACTIVITY_PVP] = 0;
		m_activityToday[ACTIVITY_GK] = 0;
		m_activityToday[ACTIVITY_OTHER_PVE] = 0;

		//...reduce activity for each day passed
		if (m_activityTotal > 0)
			m_activityTotal -= ACTIVITY_CAP / 2 * ((curTime.tm_year - lastUpdate.tm_year) * 365 + curTime.tm_yday - lastUpdate.tm_yday);

		m_activityTotal = std::max(0, m_activityTotal);

		//Mark last update
		m_lastUpdate = timestamp;

		//Mark to save
		m_doSave = true;
	}
}

void ActivityHandler::SyncShopOfflineTime()
{
	TPacketGCShopOffTimeSync pack;
	pack.bHeader = HEADER_GC_SYNC_SHOP_OFFTIME;
	pack.value = GetOfflineShopMinutesFromActivity(m_activityTotal, m_player->GetPremiumRemainSeconds(PREMIUM_SHOP_DOUBLE_UP) > 0);
	GetPlayer()->GetDesc()->Packet(&pack, sizeof(TPacketGCShopOffTimeSync));
}

bool ActivityHandler::CountsForActivity(LPCHARACTER killer, LPCHARACTER victim)
{
	if (!killer->IsPC() || !victim->IsPC())
		return false;

	auto it = killMap.find(killer->GetPlayerID()); //Find killer in map
	if (it == killMap.end())
		return true;

	//Find victim in recent ones. Only if not found it will count
	return std::find(it->second.begin(), it->second.end(), victim->GetPlayerID()) == it->second.end();
}

void ActivityHandler::RecordKill(DWORD killerPID, DWORD victimPID)
{
	auto it = killMap.find(killerPID); //Find killer in map
	if (it == killMap.end())
	{
		killMap.insert({ killerPID, { victimPID } });
		return;
	}

	std::vector<DWORD>& victims = it->second;
	if (std::find(victims.begin(), victims.end(), victimPID) != victims.end()) //Sanity check. Should not happen.
	{
		sys_err("Trying to record activity kill even though the victim is already there.");
		return;
	}

	//Cap reached, remove the first
	if (victims.size() >= REMEMBER_PVP_KILLS)
		victims.erase(victims.begin());

	victims.push_back(victimPID);
}
#endif
